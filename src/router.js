import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import LoginPage from "./routes/LoginPage";
import RegisterUser from "./routes/RegisterUser";
import HomePage from "./routes/Home";
import Header from './components/header';

export default class App extends Component {
    state = {
        hasToken: false,
        isLoading: false
    };
    componentDidMount() {
        var localParam = localStorage.getItem('token');
        if(localParam) {
            this.setState({
                hasToken: true
            });
        }
    }
    handleLoginChange = () => {
        this.setState({
            hasToken: true
        });
    }
    handleLoading = (status) => {
        this.setState({
            isLoading: status
        });
    }
    render() {
        return (
            <Router>
                <div>
                    <Header isLoading={this.state.isLoading} isLoggedIn={this.state.hasToken}/>
                    <Switch>
                        <Route path="/login">
                            <LoginPage shouldRouteChange={this.handleLoginChange} loadingStatus={this.handleLoading}/>
                        </Route>
                        <Route path="/register">
                            <RegisterUser loadingStatus={this.handleLoading}/>
                        </Route>
                        <Route path="/">
                            <HomePage loadingStatus={this.handleLoading}/>
                        </Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}
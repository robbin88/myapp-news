import React from 'react';
import { NavLink } from "react-router-dom";
import {FaHome, FaLock} from 'react-icons/fa'
import '../styles/header.css';
import Loader from "../components/loader";

export default class Header extends React.Component {
    state = {
        loading: this.props.isLoading
    };
    handleLogout = () => {
        localStorage.clear();
        window.location.href = '/';
    }
    // shouldComponentUpdate(prevProps, nextProps) {
    //     console.log(prevProps);
    //     console.log(nextProps);
    //     return false;
    // }
    render() {
        return(
            <div>
                <nav>
                    <ul>
                        <li>
                            <NavLink activeStyle={{color: 'blue'}} exact to="/">
                                <FaHome size="1.2em" style={{marginRight: '5px', marginBottom: '5px', padding: '2px', verticalAlign: 'middle'}}/>
                                Main Page
                            </NavLink>
                        </li>
                        {(this.props.isLoggedIn) ? 
                            <span>
                                <li>
                                    <a href="/" onClick={this.handleLogout}>Logout</a>
                                </li>
                            </span> :
                            <span>
                                <li>
                                    <NavLink activeStyle={{color: 'blue'}} exact to="/login">
                                        <FaLock size="1.2em" style={{marginRight: '5px', marginBottom: '5px', padding: '2px', verticalAlign: 'middle'}}/>
                                        Login
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink activeStyle={{color: 'blue'}} exact to="/register">Register</NavLink>
                                </li>
                            </span> 
                        }
                    </ul>
                </nav>
                {(this.state.loading) ? 
                <Loader/> : null    
                }
            </div>
        );
    }
}
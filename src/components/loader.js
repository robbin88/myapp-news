import React from "react";

export default class Loader extends React.Component {
    render() {
        return(
            <div id="loader">
                <div className="one"></div>
                <div className="two"></div>
                <div className="three"></div>
                <div className="four"></div>
                <div className="five"></div>
                <div className="six"></div>
                <div className="seven"></div>
            </div>
        );
    }
}
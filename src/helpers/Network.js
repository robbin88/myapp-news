import axios from "axios";

const baseUrl = 'http://www.robin-vashisht.com:8082/';

const Request = {
    loginUser: function(data) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                url: baseUrl+'users/login',
                data: data,
            }).then(resp => {
                resolve(resp);
            }).catch(err => {
                reject(err);
            })
        })
    },
    registerUser: function(data) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                url: baseUrl+'users/register',
                data: data
            }).then(resp => {
                resolve(resp);
            }).catch(err => {
                reject(err);
            })
        })
    },
    getUserDetails: function() {
        return new Promise((resolve, reject) => {
            axios({
                method: 'GET',
                url: baseUrl+'users/',
                headers: {
                    'X-AUTH-TOKEN': localStorage.getItem('token') 
                }
            }).then(resp => {
                resolve(resp);
            }).catch(err => {
                reject(err);
            })
        })
    },
    getNews: function() {
        return new Promise((resolve, reject) => {
            axios({
                method: 'GET',
                url: 'http://www.robin-vashisht.com:8081/',
                headers: {
                    'X-AUTH-TOKEN': localStorage.getItem('token') 
                }
            }).then(resp => {
                resolve(resp);
            }).catch(err => {
                reject(err);
            })
        })
    }
}

export default Request;
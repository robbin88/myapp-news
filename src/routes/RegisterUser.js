import React from "react";
import Request from '../helpers/Network';
import '../styles/register.css';

export default class RegisterUser extends React.Component {
    state = {
        username: '',
        emailId: '',
        password: '',
        confirmpassword: '',
        responseData: []
    };
    usernameValue = (e) => {
        let value = e.target.value;
        this.setState({
            username: value
        });
    }
    passwordValue = (e) => {
        let value = e.target.value;
        this.setState({
            password: value
        });
    }
    confirmPasswordValue = (e) => {
        let value = e.target.value;
        this.setState({
            confirmpassword: value
        });
    }
    emailIdValue = (e) => {
        let value = e.target.value;
        this.setState({
            emailId: value
        });
    }
    submitForm = () => {
        let {username, password, emailId} = this.state;
        Request.registerUser({username: username, password: password, email: emailId, contact: ''}).then(resp => {
            console.log(resp.data);
            this.setState({
                responseData: resp.data
            });
            if(resp.data.status === 'success') {
                localStorage.setItem('token', resp.data.token);
            } else {
                alert('Error Occurred');
            }
        }, err => {
            console.log(err);
        });
    }
    render() {
        return(
            <div>
                <h4>
                    Register Page
                </h4>
                <div className="registerBox">
                    <div>
                        <label>USERNAME</label>
                        <input type="text" onChange={this.usernameValue}/>
                    </div>
                    <div>
                        <label>EMAIL-ID</label>
                        <input type="text" onChange={this.emailIdValue}/>
                    </div>
                    <div>
                        <label>PASSWORD</label>
                        <input type="password" onChange={this.passwordValue}/>
                    </div>
                    <div>
                        <label>CONFIRM PASSWORD</label>
                        <input type="password" onChange={this.confirmPasswordValue}/>
                    </div>
                    <div>
                        <button onClick={this.submitForm}>SUBMIT</button>
                    </div>
                </div>
                {(this.state.responseData != null) ? 
                    <code>
                        {JSON.stringify(this.state.responseData)}
                    </code> : null
                }
            </div>
        );
    }
}
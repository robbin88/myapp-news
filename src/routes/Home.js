import React from "react";
import Request from "../helpers/Network";

export default class Home extends React.Component {
    state = {
        responseData: [],
        newsList: []
    };
    componentDidMount() {
        Request.getUserDetails().then(resp => {
            this.setState({
                responseData: resp.data
            });
            console.log(resp);
        }, err => {
            console.log(err);
        });
        this.getNews();
    }
    getNews = () => {
        this.props.loadingStatus(true);
        Request.getNews().then(resp => {
            console.log(resp);
            if(resp.data.status === 'success') {
                this.setState({
                    newsList: resp.data.data
                });
            } else {
                alert('Error');
            }
            this.props.loadingStatus(false);
        }, err => {
            this.props.loadingStatus(false);
            console.log(err);
        });
    }
    render() {
        let {newsList} = this.state; 
        return(
            <div>
                <ul style={{padding: 0}}>
                {(newsList.length > 0) ? 
                        newsList.map((item, index) => {
                            if(item.title !== '') {
                                return (
                                    <li key={index} className="listItem">
                                        <div className="listContentBox">
                                            <h4>
                                                {item.title}
                                            </h4>
                                            <p>
                                                {item.desc}
                                            </p>
                                        </div>
                                    </li>
                                );
                            }
                        })
                     : null
                }
                </ul>
            </div>
        );
    }
}
import React from "react";
import Request from "../helpers/Network";
import '../styles/forms.css';
import '../styles/login.css';

export default class LoginPage extends React.Component {
    state = {
        username: '',
        password: '',
        responseData: []
    };
    loginUser = () => {
        var {username, password} = this.state;
        Request.loginUser({username: username, password: password}).then(resp => {
            console.log(resp.data);
            this.setState({
                responseData: resp.data
            });
            if(resp.data.status === 'success') {
                this.props.shouldRouteChange();
                window.location.href = '/';
                localStorage.setItem('token', resp.data.token);
            } else {
                alert('Error Occurred');
            }
        }, err => {
            console.log(err);
        });
    }
    usernameValue = (e) => {
        let value = e.target.value;
        this.setState({
            username: value
        });
    }
    passwordValue = (e) => {
        let value = e.target.value;
        this.setState({
            password: value
        });
    }
    render() {
        return(
            <div>
                <h4>
                    Login Page
                </h4>
                <div className="formBox">
                    <div>
                        <label>USERNAME</label>
                        <input type="text" onChange={this.usernameValue}/>
                    </div>
                    <div>
                        <label>PASSWORD</label>
                        <input type="password" onChange={this.passwordValue}/>
                    </div>
                    <div>
                        <button onClick={this.loginUser}>SUBMIT</button>
                    </div>
                </div>
                {(this.state.responseData != null) ? 
                    <code>
                        {JSON.stringify(this.state.responseData)}
                    </code> : null
                }
            </div>
        );
    }
}